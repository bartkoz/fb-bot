# Super simple messenger bot with Watson AI


What is needed:

```
pip install watson-developer-cloud
pip install fbchat
pip install python-decouple
```
All confidential data is hidden under python-decouple. Variables are:
+ FB_USER
+ FB_PASSWORD
+ WATSON_USER
+ WATSON_PASSWORD
To turn bot on:
```python fb-bot.py```
